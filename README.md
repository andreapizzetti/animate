# animate

A bunch of codes and scripts to create a realistic animation of the attitude dynamics of a satellite. A STL object of the satellite shape is required. 
An example of visualization can be found here: https://www.youtube.com/watch?v=_5tUXtlYHwk&ab_channel=AndreaPizzetti
