function [] = satellite_3d_animation(...
            model_mat_file, ...         
            quaternion,...
            trajectory,...
            time,...
            time_label,...
            vectors,...
            vectors_labels,...
            signals,...
            signals_labels,...
            options)  

addpath models textures

%% EXTRACTING DATA
% orientation
euler = quat_to_euler(quaternion);
roll= euler(1,:);
pitch = euler(2,:);
yaw = euler(3,:);

% position
if isempty(trajectory)
    flag_traj=false;
else
    flag_traj=true;
    r = trajectory;
end

%% RE-SAMPLING
% define the reproduction speed factor
speedx = options.movie.speed*30*time(end)/3600; 
% The frame sample time shall be higher than 0.02 seconds to be able to 
% update the figure (CPU/GPU constraints)
frame_sample_time = max(0.02, time(2)-time(1));
% Resample the time vector to modify the reproduction speed
t_new   = time(1):frame_sample_time*(speedx):time(end);

% ANGLES
% We have to be careful with angles with ranges
yaw_deg  = atan2(interp1(time, sin(yaw), t_new,'linear'), interp1(time, cos(yaw), t_new,'linear')) * 180 / pi;
pitch_deg  = atan2(interp1(time, sin(pitch), t_new,'linear'), interp1(time, cos(pitch), t_new,'linear')) * 180 / pi;
roll_deg  = atan2(interp1(time, sin(roll), t_new,'linear'), interp1(time, cos(roll), t_new,'linear')) * 180 / pi;

% TRAJECTORY
if flag_traj
    r_new= interp1(time', r', t_new', 'linear');
end

% VECTORS
nvecs=length(vectors);
for iv=1:nvecs
    vectors{iv} = interp1(time',vectors{iv}',t_new','linear');
end

% SIGNALS
nplots=length(signals);
for ip=1:nplots
    signals{ip} = interp1(time',signals{ip}',t_new','linear');
end

% load the model
load(model_mat_file, 'Model3D');

% Get maximum dimension including all the sat parts
R = 3*max(max(sqrt(sum(Model3D.Aircraft(1).stl_data.vertices.^2, 2))));



%% FIGURE & VIDEO
hf = figure;
set(gcf, 'WindowState','Maximized');
AX = axes('position',[0.0 0.0 1 1]);
set(AX, 'color', 'none');
axis off
axis equal
axis([-1, 1, -1, 1, -1, 1] * 3.0 * R)
hold on

% Applying user-defined options to figure and axes
set(gcf, options.figure)
set(gca, options.axes)
zoom(options.zoom)

% Camera and light
cameratoolbar('Show')
camlight HEADLIGHT;

% Open the video output if we are recording the movie
if options.movie.flag == 1
    aviobj = VideoWriter(options.movie.file_name, 'MPEG-4');
    aviobj.Quality = 100;  % movie quality
    aviobj.FrameRate = 1/frame_sample_time;
    open(aviobj);
end

%% TRANSFORMATION HANDLES

AV_hg         = hgtransform;

% Circles around the sat transformation group handles
euler_hgt(1)  = hgtransform('Parent',           AX, 'tag', 'OriginAxes');
euler_hgt(2)  = hgtransform('Parent', euler_hgt(1), 'tag', 'dyn_frame');
% YAW - BLUE
euler_hgt(3)  = hgtransform('Parent', euler_hgt(1), 'tag', 'yaw_disk');
euler_hgt(4)  = hgtransform('Parent', euler_hgt(3), 'tag', 'yaw_line');
% PITCH - GREEN
euler_hgt(5)  = hgtransform('Parent', euler_hgt(4), 'tag', 'pitch_disk');
euler_hgt(6)  = hgtransform('Parent', euler_hgt(5), 'tag', 'pitch_line');
% ROLL - RED
euler_hgt(7)  = hgtransform('Parent', euler_hgt(6), 'tag', 'roll_disk');
euler_hgt(8)  = hgtransform('Parent', euler_hgt(7), 'tag', 'roll_line');

%% GRAPHIC OBJECTS GENERATION

% -- BODY -- %
for i = 1:length(Model3D.Aircraft)
    AV = patch(Model3D.Aircraft(i).stl_data,  ...
        'FaceColor',         Model3D.Aircraft(i).color, ...
        'EdgeColor',        'black',        ...
        'FaceLighting',     'gouraud',     ...
        'AmbientStrength',   0.15,         ...
        'FaceAlpha',         Model3D.Aircraft(i).alpha,...
        'LineSmoothing',    'on',...
        'Parent',            AV_hg(1));
end

% -- CIRCLES -- %
phi = (-pi:pi/36:pi)';
D1 = [sin(phi) cos(phi) zeros(size(phi))];
% yaw disk has rotation axis aligned with +Z
plot3(R * D1(:,1), R * D1(:,2), R * D1(:,3), 'Color', 'b', 'LineWidth',2, 'tag', 'yaw_disk', 'Parent', euler_hgt(3));
% pitch disk has rotation axis aligned with +Y_yawDisk
plot3(R * D1(:,1), R * D1(:,3), R * D1(:,2), 'Color', 'g', 'LineWidth',2,'tag', 'pitch_disk', 'Parent', euler_hgt(5));
% roll disk has rotation axis aligned with +X_pitchDisk
plot3(R * D1(:,3), R * D1(:,1), R * D1(:,2), 'Color', 'r', 'LineWidth',2,'tag', 'roll_disk', 'Parent', euler_hgt(7));

% -- MARKS -- %
% Plot +0,+90,+180,+270 Marks
S = 0.95;
phi = -pi+pi/2:pi/2:pi;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'yaw_disk', 'Parent',euler_hgt(3));
plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(3, :); R * D1(3, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'g', 'tag', 'pitch_disk', 'Parent',euler_hgt(5));
plot3([S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'r', 'tag', 'roll_disk', 'Parent',euler_hgt(7));
% Plot +45,+135,+180,+225,+315 Marks
S = 0.95;
phi = -pi+pi/4:2*pi/4:pi;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'yaw_disk', 'Parent',euler_hgt(3));
plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(3, :); R * D1(3, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'g', 'tag', 'pitch_disk', 'Parent',euler_hgt(5));
plot3([S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'r', 'tag', 'roll_disk', 'Parent',euler_hgt(7));
% 10 deg sub-division marks
S = 0.98;
phi = -180:10:180;
phi = phi*pi / 180;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'yaw_disk', 'Parent', euler_hgt(3));
plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(3, :); R * D1(3, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'g', 'tag', 'pitch_disk', 'Parent', euler_hgt(5));
plot3([S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'r', 'tag', 'roll_disk', 'Parent', euler_hgt(7));

% -- GUIDE LINES -- %
% yaw line is aligned with +X_yawDisk
plot3([-R, R], [ 0, 0], [ 0, 0], 'b-', 'LineWidth', 2,'tag','yaw_line', 'parent', euler_hgt(4));
% pitch line is aligned with +Z_pitchDisk
plot3([ 0, 0], [ 0, 0], [-R ,R], 'g-', 'LineWidth', 2,'tag','pitch_line', 'parent', euler_hgt(6));
% roll line is aligned with +y
plot3([ 0, 0], [-R, R], [ 0, 0], 'r-', 'LineWidth', 2,'tag','roll_line', 'parent', euler_hgt(8));

% -- ANGLES -- %
% Yaw, pitch and roll angles visualized
hdle_yaw = plot3(0,0,0, 'b--','XDataSource', 'X_yaw', 'YDataSource', 'Y_yaw', 'ZDataSource', 'Z_yaw', 'linewidth', 0.3);
hdle_pitch = plot3(0,0,0, 'g--', 'XDataSource', 'X_pitch', 'YDataSource', 'Y_pitch', 'ZDataSource', 'Z_pitch', 'linewidth', 0.3);
hdle_roll = plot3(0,0,0, 'r--', 'XDataSource', 'X_roll', 'YDataSource', 'Y_roll', 'ZDataSource', 'Z_roll', 'linewidth', 0.3);

% -- INERTIAL FRAME -- %
c1=1.2*R;
z=zeros(3,1);
quiver3(z,z,z,[c1;0;0],[0;c1;0],[0;0;c1],'k')
text([c1;0;0],[0;c1;0],[0;0;c1],{'X','Y','Z'},'Fontsize',14,'color','k','HorizontalAlign','center','VerticalAlign','middle')

% -- BODY FRAME -- %
c2=0.7*R;
quiver3(z,z,z,[c2;0;0],[0;c2;0],[0;0;c2],'k','LineWidth',2,'tag','text_bf','parent',AV_hg)
text([c2;0;0],[0;c2;0],[0;0;c2],{'x','y','z'},'Color','k','LineWidth',2,'tag','bf','parent',AV_hg)

% -- VECTORS -- %
colors=[linspace(0.3,0.7,nvecs);linspace(0.7,0.3,nvecs);linspace(0.3,0.7,nvecs)];
c3=0.8*R;
for iv=1:nvecs
    vec_temp=vectors{iv};
    h_vec(iv)=quiver3(0,0,0,c3*vec_temp(1,1),c3*vec_temp(1,2),c3*vec_temp(1,3),'Color', colors(:,iv),'LineWidth',2);
    h_text(iv)= text(c3*vec_temp(1,1),c3*vec_temp(1,2),c3*vec_temp(1,3), vectors_labels{iv},'Color',colors(:,iv));
end

% -- TEXT -- %
time=t_new(1);
FontSize    = 12;
text_color  = [0, 0, 0];
font_name   = 'Palatino';
hdle_text_t                 = text(-1.05 * R * 1.5, 0.1* R * 1.5, 0.1 * R * 1.5, [time_label,' = ', num2str(time)], 'Color',text_color, 'FontSize',FontSize, 'FontName', font_name);
hdle_text_yaw               = text(-1.05 * R * 1.5, 0.1* R * 1.5, 0.2 * R * 1.5, '', 'Color','b', 'FontSize', FontSize, 'FontName', font_name);
hdle_text_pitch             = text(-1.05 * R * 1.5, 0.1* R * 1.5,  0.3 * R * 1.5, '', 'Color','g', 'FontSize', FontSize, 'FontName', font_name);
hdle_text_roll              = text(-1.05 * R * 1.5, 0.1* R * 1.5, 0.4 * R * 1.5, '', 'Color','r', 'FontSize', FontSize, 'FontName', font_name);

% -- SIGNALS -- %
hplots=0.8/nplots;
yplots=linspace(0.07,0.93-hplots,nplots);
for ip=1:nplots
    if ~isempty(options.signals.constraints)
        ix_constraints{ip}=boolean(options.signals.constraints{ip});
    else
        ix_constraints{ip}=boolean(zeros(1,length(t_new)));
    end

    sign_temp=signals{ip};
    h_ax    = axes('Position',[0.05, yplots(ip), 0.25, hplots], 'FontSize', FontSize);
    hold(h_ax, 'on');
    try plot(h_ax, t_new, sign_temp(:, ix_constraints{ip}),'k--');
    catch, end
    plot(h_ax, t_new, sign_temp(:, ~ix_constraints{ip}));
    h_plot{ip} = plot(h_ax, t_new(1), sign_temp(1,~ix_constraints{ip}), 'ko', 'XDataSource', 'time_plot', 'YDataSource', ['signal_',num2str(ip)]);
    grid on
    xlim([t_new(1), t_new(end)])
    hold all; box on;
    if ip==1
       xlabel(time_label)
    else
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
    end
    ylabel(signals_labels{ip})
end

% -- TRAJECTORY -- %
if flag_traj

% Planet
R_planet=6378e3;
h_traj    = axes('Position',[0.55, 0.2, 0.6, 0.6], 'FontSize', FontSize);
hold on
axis equal
axis off
axis([-1, 1, -1, 1, -1, 1] * (norm(r_new(1,:))))
view(AX.View)
np = 39;
[X_E,Y_E,Z_E] = sphere(np-1);
h_earth = surf(h_traj, R_planet*X_E,R_planet*Y_E,R_planet*Z_E,'FaceColor','none','EdgeColor',0.5*[1 1 1], 'HandleVisibility','off');
cdata=imread('EarthTexture.jpg'); % Load Earth image for texture map
% Set image as color data (cdata) property, and set face color to indicate
% a texturemap, which Matlab expects to be in cdata.
h_earth.FaceColor = 'texturemap';
h_earth.CData = flip(cdata); % W/o flip() the earth texture looks upside down
h_earth.EdgeColor = 'none';
h_earth.FaceAlpha = 0.6;
% trajectory
plot3(h_traj, r_new(:,1), r_new(:,2), r_new(:,3),'c','LineWidth',3);
% point
h_sc = plot3(h_traj, r_new(1,1), r_new(1,2), r_new(1,3),'mo', 'LineWidth',3,'XDataSource', 'X_sc', 'YDataSource', 'Y_sc','ZDataSource','Z_sc');
end
%% ANIMATION

% Refresh plot for flight visualization
tic;
for i=1:length(yaw_deg)
 
    % AIRCRAFT BODY
    M1 = makehgtform('zrotate',  yaw_deg(i) * pi / 180);  % yaw rotation
    M2 = makehgtform('yrotate', pitch_deg(i) * pi / 180);  % pitch rotation
    M3 = makehgtform('xrotate', roll_deg(i) * pi / 180);  % roll rotation
    set(AV_hg, 'Matrix', M1 * M2 * M3)
    
    % Yaw line
    M = makehgtform('zrotate', yaw_deg(i) * pi / 180);
    set(euler_hgt(4), 'Matrix', M)

    % Pitch line
    M = makehgtform('yrotate', pitch_deg(i) * pi / 180);
    set(euler_hgt(6), 'Matrix', M)   
   
    % Roll line
    M = makehgtform('xrotate', roll_deg(i) * pi / 180);
    set(euler_hgt(8), 'Matrix', M)
    
    % time
    time = (i-1)*frame_sample_time*speedx;

    % Angles visualized
    k=0.9;
    n=10;
    cart=cart_coord([k*R*ones(1,n); pi/180*yaw_deg(i)*linspace(0,1,n); zeros(1,n)]);
    X_yaw = cart(1,:);
    Y_yaw  = cart(2,:);
    Z_yaw  = -cart(3,:);
    cart=cart_coord([k*R*ones(1,n); pi/180*yaw_deg(i)*ones(1,n); pi/2-pi/180*pitch_deg(i)*linspace(0,1,n)]);
    X_pitch = cart(1,:);
    Y_pitch  = cart(2,:);
    Z_pitch  = cart(3,:);
    dcm=k*R*euler_to_dcm([pi/180*roll_deg(i)*linspace(0,1,n); pi/180*pitch_deg(i)*ones(1,n); pi/180*yaw_deg(i)*ones(1,n)]);
    X_roll = dcm(2,1,:);
    Y_roll = dcm(2,2,:);
    Z_roll = dcm(2,3,:);
    refreshdata(hdle_yaw, 'caller')
    refreshdata(hdle_pitch, 'caller')
    refreshdata(hdle_roll, 'caller')

    % Refresh  texts
    set(hdle_text_t, 'String',sprintf([time_label,' = %3.2f'],time))
    set(hdle_text_yaw, 'String',strcat('yaw= ',num2str(yaw_deg(i), '%2.1f'), ' deg'))
    set(hdle_text_pitch, 'String',strcat('pitch= ',num2str(pitch_deg(i), '%2.1f'), ' deg'))
    set(hdle_text_roll, 'String',strcat('roll= ',num2str(roll_deg(i), '%2.1f'), ' deg'))

    % VECTORS UPDATE
    for iv=1:nvecs
        vec_temp=c3*vectors{iv};
        h_vec(iv).UData=vec_temp(i,1);
        h_vec(iv).VData=vec_temp(i,2);
        h_vec(iv).WData=vec_temp(i,3);
        set(h_text(iv),'Position',[vec_temp(i,1) vec_temp(i,2) vec_temp(i,3)]);
    end

    % PLOTS UPDATE
    for ip=1:nplots
        signal_temp=signals{ip};
        time_plot=repmat(time, 1, sum(~ix_constraints{ip}));
        set(h_plot{ip},'XData',time_plot,'YData',signal_temp(i,~ix_constraints{ip}))
    end

    % TRAJECTORY UPDATE
    if flag_traj
    X_sc = r_new(i,1);
    Y_sc = r_new(i,2);
    Z_sc = r_new(i,3);
    refreshdata(h_sc,'caller')
    end

    % Real-time
    drawnow;
    if frame_sample_time * i - toc > 0 && options.realtime
        pause(max(0, frame_sample_time * i - toc))
    end
    
    % Write movie
    if options.movie.flag == 1
        writeVideo(aviobj, getframe(hf));
    end

end
toc
if options.movie.flag == 1
    close(aviobj);
end

%% SUPPORTING FUNCTIONS

function cart_coord = cart_coord(sph_coord)
%convert spherical coordinate in cartesian coordinates
if ~(size(sph_coord,1)==3 )
    error('Input must be 3 x N')
end

r = sph_coord(1,:,:);
az = sph_coord(2,:,:);
el = sph_coord(3,:,:);

cart_coord  = [ r.*cos(el).*cos(az)
                r.*cos(el).*sin(az)
                r.*sin(el)];
end

function dcm = euler_to_dcm(euler)
% This function converts from euler angles [3 x n-set-of-angles] to a direct rotation matrix
% Input: Euler angles according to p.sequence convention [rad]
% Outputs: rotation matrix (3 x 3 x n-set-of-angles)
% The angles in input must be ordered according to the convention
% RPY: [roll,pitch,yaw] (Default)

if size(euler,1)~=3
    error('Input must have 3 rows')
end

n_ang = size(euler,2);
        
c_roll = reshape(cos(euler(1,:)),1,1,n_ang);
s_roll = reshape(sin(euler(1,:)),1,1,n_ang);
c_pitch = reshape(cos(euler(2,:)),1,1,n_ang);
s_pitch = reshape(sin(euler(2,:)),1,1,n_ang);
c_yaw = reshape(cos(euler(3,:)),1,1,n_ang);
s_yaw = reshape(sin(euler(3,:)),1,1,n_ang);

dcm = [ c_pitch .* c_yaw     ,   c_pitch .* s_yaw     ,   -s_pitch ; ...
    s_roll .* s_pitch .* c_yaw - c_roll .* s_yaw , s_roll .* s_pitch .* s_yaw + c_roll .* c_yaw , c_pitch .* s_roll ; ...
    c_roll .* s_pitch .* c_yaw + s_roll .* s_yaw , c_roll .* s_pitch .* s_yaw - s_roll .* c_yaw , c_pitch .* c_roll ];    
        
end

function euler = quat_to_euler(q)
% Compute set od euler angles corresponding to the set of quaternions q
% according to the convention in p.sequence
% q can be 4 x N, euler = [ang3, ang2, ang1] shall be 3 x N
% The angles returned are ordered according to the convention
% RPY: [roll,pitch,yaw] (Default)
% REMARK: RPY and ZYX espress the same set of angles with the only difference
% of the order of the angles in the returned output

if size(q,1)~=4
    error('Input must have 4 rows')
end

q0 = q(1,:);
q1 = q(2,:);
q2 = q(3,:);
q3 = q(4,:);
sr_cp = 2*(q0.*q1 + q2.*q3);
cr_cp = 1-2*(q1.^2 + q2.^2);
sp = 2*(q0.*q2 - q3.*q1);
sy_cp = 2*(q0.*q3 + q1.*q2);
cy_cp = 1-2*(q2.^2 + q3.^2);

r = atan2(sr_cp,cr_cp);        
p = asin(sp);
y = atan2(sy_cp,cy_cp);        

euler = [r;p;y];
end

end
