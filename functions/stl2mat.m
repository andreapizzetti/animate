function Model3D=stl2mat(name, n_stl, list_stl, colors_stl, alphas_stl, offsets_stl)

% Add path to stlTools
addpath stlTools stlFiles models
% You can download the package for free from: 
% https://es.mathworks.com/matlabcentral/fileexchange/51200-stltools

% Construct the main body
for i = 1:n_stl
    Model3D.Aircraft(i).model = list_stl{i};
    Model3D.Aircraft(i).color = colors_stl{i};
    Model3D.Aircraft(i).alpha = alphas_stl(i);
    % Read the *.stl file
   [Model3D.Aircraft(i).stl_data.vertices, Model3D.Aircraft(i).stl_data.faces, ~, Model3D.Aircraft(i).label] = stlRead(list_stl{i});
    Model3D.Aircraft(i).stl_data.vertices  = Model3D.Aircraft(i).stl_data.vertices - repmat(offsets_stl(i,:),size(Model3D.Aircraft.stl_data.vertices,1),1);
end

% Set the name of the mat file containing all the info of the 3D model
mat_filepath = ['models\',name,'.mat'];
% Save mat file
save(mat_filepath, 'Model3D');

%% Check the results
% Get maximum dimension to plot the circles afterwards
AC_DIMENSION = 3*max(max(sqrt(sum(Model3D.Aircraft(1).stl_data.vertices.^2,2))));
for i=2:length(Model3D.Aircraft)
    AC_DIMENSION = max(AC_DIMENSION,max(max(sqrt(sum(Model3D.Aircraft(i).stl_data.vertices.^2,2)))));
end

iSaveMovie = 0;
AX = axes('position',[0.0 0.0 1 1]);
axis off
scrsz = get(0,'ScreenSize');
set(gcf,'Position',[scrsz(3)/40 scrsz(4)/12 scrsz(3)/2*1.0 scrsz(3)/2.2*1.0]*(1-0.0*iSaveMovie),'Visible','on');
set(AX,'color','none');
axis('equal')
hold on;
cameratoolbar('Show')
% Initializate rotations handles
% -------------------------------------------------------------------------
AV_hg(1)      = hgtransform;

euler_hgt(1)  = hgtransform('Parent',AX,'tag','OriginAxes');
euler_hgt(2)  = hgtransform('Parent',euler_hgt(1),'tag','roll_disc');
euler_hgt(3)  = hgtransform('Parent',euler_hgt(1),'tag','pitch_disc');
euler_hgt(4)  = hgtransform('Parent',euler_hgt(1),'tag','heading_disc');
euler_hgt(5)  = hgtransform('Parent',euler_hgt(2),'tag','roll_line');
euler_hgt(6)  = hgtransform('Parent',euler_hgt(3),'tag','pitch_line');
euler_hgt(7)  = hgtransform('Parent',euler_hgt(4),'tag','heading_line');

% Plot objects
% -------------------------------------------------------------------------
for i = 1:length(Model3D.Aircraft)
    AV = patch(Model3D.Aircraft(i).stl_data,  ...
        'FaceColor',         Model3D.Aircraft(i).color, ...
        'EdgeColor',        'none',        ...
        'FaceLighting',     'gouraud',     ...
        'AmbientStrength',   0.15,         ...
        'FaceAlpha',         Model3D.Aircraft(i).alpha,...
        'LineSmoothing',    'on',...
        'Parent',            AV_hg(1));
end

% Fixing the axes scaling and setting a nice view angle
axis('equal');
axis([-1 1 -1 1 -1 1] * 2.0 * AC_DIMENSION)
set(gcf,'Color',[1 1 1])
axis off
view([135,-30])
zoom(2.0);
% Add a camera light, and tone down the specular highlighting
camlight HEADLIGHT;
material('dull');

% --------------------------------------------------------------------
% Define the radius of the sphere
R = 1.0 * AC_DIMENSION;

% Outer circles
phi = (-pi:pi/36:pi)';
D1 = [sin(phi) cos(phi) zeros(size(phi))];
HP(1) = plot3(R*D1(:,1),R*D1(:,2),+R*D1(:,3),'Color','b','tag','Zplane','Parent',euler_hgt(4), 'LineWidth', 2);
HP(2) = plot3(R*D1(:,2),R*D1(:,3),+R*D1(:,1),'Color',[0 0.8 0],'tag','Yplane','Parent',euler_hgt(3), 'LineWidth', 2);
HP(3) = plot3(R*D1(:,3),R*D1(:,1),+R*D1(:,2),'Color','r','tag','Xplane','Parent',euler_hgt(2), 'LineWidth', 2);

% +0,+90,+180,+270 Marks
S = 0.95;
phi = -pi+pi/2:pi/2:pi;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
D2 = [cos(phi); zeros(size(phi)); sin(phi)];
plot3([S*R*D1(1,:); R*D1(1,:)],[S*R*D1(2,:); R*D1(2,:)],[S*R*D1(3,:); R*D1(3,:)],'Color','b','tag','Zplane','Parent',euler_hgt(4), 'LineWidth', 2);
plot3([S*R*D1(2,:); R*D1(2,:)],[S*R*D1(3,:); R*D1(3,:)],[S*R*D1(1,:); R*D1(1,:)],'Color',[0 0.8 0],'tag','Yplane','Parent',euler_hgt(3), 'LineWidth', 2);
plot3([S*R*D1(3,:); R*D1(3,:)],[S*R*D1(1,:); R*D1(1,:)],[S*R*D1(2,:); R*D1(2,:)],'Color','r','tag','Xplane','Parent',euler_hgt(2), 'LineWidth', 2);
text(R*1.05*[1;0;0],R*1.05*[0;1;0],R*1.05*[0;0;1],{'x','y','z'},'Fontsize',14,'color',[0 0 0],'HorizontalAlign','center','VerticalAlign','middle');

% +45,+135,+180,+225,+315 Marks
S = 0.95;
phi = -pi+pi/4:2*pi/4:pi;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S*R*D1(1,:); R*D1(1,:)],[S*R*D1(2,:); R*D1(2,:)],[S*R*D1(3,:); R*D1(3,:)],'Color','b','tag','Zplane','Parent',euler_hgt(4), 'LineWidth', 2);

% 10 deg sub-division marks
S = 0.98;
phi = -[0:10:90 80:-10:0 -10:-10:-90 -80:10:0];
PHI_TEXT{length(phi)}='';
for i=1:length(phi)
    PHI_TEXT{i} = num2str(phi(i));
end
theta_t = -[0:10:90 80:-10:0 -10:-10:-90 -80:10:0];
THETA_TEXT{length(theta_t)}='';
for i=1:length(theta_t)
    THETA_TEXT{i} = num2str(theta_t(i));
end
phi = -180:10:180;
phi = phi*pi/180;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S*R*D1(1,:); R*D1(1,:)],[S*R*D1(2,:); R*D1(2,:)],[S*R*D1(3,:); R*D1(3,:)],'Color','b','tag','Zplane','Parent',euler_hgt(4), 'LineWidth', 2);
plot3([S*R*D1(2,:); R*D1(2,:)],[S*R*D1(3,:); R*D1(3,:)],[S*R*D1(1,:); R*D1(1,:)],'Color',[0 0.8 0],'tag','Yplane','Parent',euler_hgt(3), 'LineWidth', 2);
plot3([S*R*D1(3,:); R*D1(3,:)],[S*R*D1(1,:); R*D1(1,:)],[S*R*D1(2,:); R*D1(2,:)],'Color','r','tag','Xplane','Parent',euler_hgt(2), 'LineWidth', 2);

% Plot guide lines
HL(1) = plot3([-R R],[0 0],[0 0],'b-','tag','heading_line','parent',euler_hgt(7), 'LineWidth', 2);
HL(2) = plot3([-R R],[0 0],[0 0],'g-','tag','pitch_line','parent',euler_hgt(6),'color',[0 0.8 0], 'LineWidth', 2);
HL(3) = plot3([0 0],[-R R],[0 0],'r-','tag','roll_line','parent',euler_hgt(5), 'LineWidth', 2);

end