clear
clc
close all

addpath functions

cp_install()

%% IMPORT MODEL
name='CubeSat';
% number of rigid body elements
n_stl = 1;
% Define the list of parts which will be part of the rigid aircraft body
list_stl   = {'CubeSat.stl'};
% Define the color of each part
colors_stl = { 0.2 * [1, 1, 1]};
% Define the transparency of each part
alphas_stl            = [0.9];
% Define the model offsets to center the Center of Gravity
offsets_stl(1,:)   = [100; 
                      100; 
                      200];

mat=stl2mat(name, n_stl, list_stl, colors_stl, alphas_stl, offsets_stl);

%% ANIMATION
n=1000;

t=linspace(0,100,n);
time_label='seconds [s]'
roll=pi/100*t;
pitch=pi/6*ones(1,n);
yaw=pi/100*t;
euler=[roll; pitch; yaw];
q=cp_euler_to_quat(euler);
r=[];

h=ones(3,n);
h_b=ones(3,n);
w=zeros(3,n);
w_w=zeros(3,n);
I=[40,  0,  0;
    0, 40,  0;
    0,  0, 20];
ape=zeros(1,n);

model_mat_file=[name,'.mat'];

options.figure.Color=[1, 1, 1];
options.realtime=false;

options.zoom=3;

options.axes.View=[45 45];
options.axes.CameraPosition=[1e3 1e3 0.5e3];

options.movie.flag=0;
options.movie.speed=2;
options.movie.file_name='test';

satellite_3d_animation(...
    model_mat_file, ...     
    q,...
    r,...
    t,...
    time_label,...
    {h},...
    {'h'},...
    {w*180/pi, w_w*180/pi*60/360},...
    {'\omega [deg/s]','\omega_w [rpm]'},...
    options)  